# tboreux.gitlab.io 🌱

![MIT License](https://img.shields.io/gitlab/license/tboreux%2Ftboreux.gitlab.io?style=flat-square&logo=gitlab&logoColor=white&color=4CAF50) ![GitLab Language Count](https://img.shields.io/gitlab/languages/count/tboreux%2Ftboreux.gitlab.io?style=flat-square&logo=gnometerminal&logoColor=white&color=4CAF50) ![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/tboreux%2Ftboreux.gitlab.io?style=flat-square&logo=git&logoColor=white&color=4CAF50) ![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/tboreux%2Ftboreux.gitlab.io?branch=main&style=flat-square&logo=rocket&logoColor=white&color=4CAF50)

This website is my professional portfolio.

## Demo

This website is accessible from [tboreux.gitlab.io](https://tboreux.gitlab.io), feel free to visit and test it! 

But you'll find in the next section some screenshots.


## Screenshots

### Home

![App Screenshot](screenshots/01.home.png)

### About

![App Screenshot](screenshots/02.about.png)

### Skills

![App Screenshot](screenshots/03.skills.png)

### Resume

![App Screenshot](screenshots/04.resume.png)


## Tech Stack

![Static Badge](https://img.shields.io/badge/HTML-v5-4CAF50?style=flat-square&logo=html5&logoColor=white) ![Static Badge](https://img.shields.io/badge/CSS-v3-4CAF50?style=flat-square&logo=css3&logoColor=white) ![Static Badge](https://img.shields.io/badge/Javascript-ES2023-4CAF50?style=flat-square&logo=javascript&logoColor=white)

## Usage

To use this project:

1. Clone this repo: `git clone git@gitlab.com:tboreux/tboreux.gitlab.io.git`.
2. Modify the `public/index.html` file with your informations.
5. Host your `public` directory on your webserver.

## License

[![MIT License](https://img.shields.io/badge/License-MIT-4CAF50?style=flat-square)](https://choosealicense.com/licenses/mit/)
